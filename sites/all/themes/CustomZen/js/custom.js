

 (function ($) {
    $(document).ready(function() {
        // Open PDF In New Window
        $('a[href$=".pdf"]').attr('target', 'new');
    });

}(jQuery)); 