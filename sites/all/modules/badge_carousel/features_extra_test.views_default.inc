<?php
/**
 * @file
 * features_extra_test.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function features_extra_test_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'badge_carousel';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Badge Carousel';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Badge Carousel';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'jcarousel';
  $handler->display->display_options['style_options']['wrap'] = '0';
  $handler->display->display_options['style_options']['visible'] = '5';
  $handler->display->display_options['style_options']['auto'] = '0';
  $handler->display->display_options['style_options']['autoPause'] = 1;
  $handler->display->display_options['style_options']['easing'] = '';
  $handler->display->display_options['style_options']['vertical'] = 0;
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['content'] = '<h1>Services\' SAPR/SHARP Offices</h1>';
  $handler->display->display_options['header']['area']['format'] = 'filtered_html';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: Content: Badge Upload */
  $handler->display->display_options['fields']['field_badge_upload']['id'] = 'field_badge_upload';
  $handler->display->display_options['fields']['field_badge_upload']['table'] = 'field_data_field_badge_upload';
  $handler->display->display_options['fields']['field_badge_upload']['field'] = 'field_badge_upload';
  $handler->display->display_options['fields']['field_badge_upload']['label'] = '';
  $handler->display->display_options['fields']['field_badge_upload']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_badge_upload']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_badge_upload']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['field_badge_upload']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_badge_upload']['settings'] = array(
    'image_style' => 'thumbnail',
    'image_link' => '',
  );
  $handler->display->display_options['fields']['field_badge_upload']['group_rows'] = FALSE;
  $handler->display->display_options['fields']['field_badge_upload']['delta_offset'] = '0';
  /* Filter criterion: Content: Badge Upload (field_badge_upload:alt) */
  $handler->display->display_options['filters']['field_badge_upload_alt']['id'] = 'field_badge_upload_alt';
  $handler->display->display_options['filters']['field_badge_upload_alt']['table'] = 'field_data_field_badge_upload';
  $handler->display->display_options['filters']['field_badge_upload_alt']['field'] = 'field_badge_upload_alt';
  $handler->display->display_options['filters']['field_badge_upload_alt']['operator'] = 'contains';

  /* Display: Services' SAPR/SHARP Offices */
  $handler = $view->new_display('page', 'Services\' SAPR/SHARP Offices', 'page');
  $handler->display->display_options['path'] = 'badge-carousel';

  /* Display: Services' SAPR/SHARP Offices */
  $handler = $view->new_display('block', 'Services\' SAPR/SHARP Offices', 'block');
  $export['badge_carousel'] = $view;

  return $export;
}
