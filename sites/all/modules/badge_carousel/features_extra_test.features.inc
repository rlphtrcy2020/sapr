<?php
/**
 * @file
 * features_extra_test.features.inc
 */

/**
 * Implements hook_views_api().
 */
function features_extra_test_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_fe_nodequeue_export_fields().
 */
function features_extra_test_fe_nodequeue_export_fields() {
  $nodequeues = array();

  // Exported nodequeues: features_extra_test_nodequeue
  $nodequeues['features_extra_test_nodequeue'] = array(
    'name' => 'features_extra_test_nodequeue',
    'title' => 'Features Extra Test Nodequeue',
    'subqueue_title' => '',
    'size' => 0,
    'link' => '',
    'link_remove' => '',
    'owner' => 'nodequeue',
    'show_in_ui' => 1,
    'show_in_tab' => 1,
    'show_in_links' => 0,
    'reference' => 0,
    'reverse' => 0,
    'insert_at_front' => NULL,
    'i18n' => 0,
    'subqueues' => 1,
    'types' => array(
      0 => 'article',
    ),
    'roles' => array(),
    'count' => 0,
  );

  return $nodequeues;
}

/**
 * Implements hook_node_info().
 */
function features_extra_test_node_info() {
  $items = array(
    'badge_carousel' => array(
      'name' => t('badge_carousel'),
      'base' => 'node_content',
      'description' => t('Carousel of Badges'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
